# immocloud-bewerber

Dies ist das Frontend für die Aufgabe. Es wurde mit der neuesten Version von Vue (mit Typescript) erstellt und verfügt über alle erforderlichen Funktionen gemäß Anleitung.
Außerdem kommuniziert dieses Frontend bei Bedarf über die API mit dem Backend (Spring Boot), um „Bewerber“ hinzuzufügen und deren Status zu aktualisieren.
Das Backend befindet sich in einem eigenen Repository. Um das Backend zu überprüfen, besuchen Sie bitte [hier](https://gitlab.com/Ghaz14/immocloud-backend)

## Customize configuration

See [Vite Configuration Reference](https://vitejs.dev/config/).

## Project Setup

```sh
npm install
```

### Compile and Hot-Reload for Development

```sh
npm run dev
```

### Run Unit Tests with [Vitest](https://vitest.dev/)

```sh
npm run test:unit
```

### Lint with [ESLint](https://eslint.org/)

```sh
npm run lint
```
