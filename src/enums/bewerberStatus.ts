export enum BewerberStatus {
  OPEN = 'Offen',
  ACCEPTED = 'Akzeptiert',
  DECLINED = 'Abgelehnt'
}
