import { describe, it, expect } from 'vitest'

import { mount } from '@vue/test-utils'
import BewerberTabelle from '../BewerberTabelle.vue'
import { BewerberStatus } from '../../enums/bewerberStatus'

describe('BewerberTabelle', () => {
  it('renders properly', () => {
    const wrapper = mount(BewerberTabelle, {
      props: { bewerbern: [{ id: 1, name: 'example', status: BewerberStatus.OPEN }] }
    })
    console.log(wrapper.findAll('tr'))
    expect(wrapper.findAll('tr')).toHaveLength(2)
  })
})
