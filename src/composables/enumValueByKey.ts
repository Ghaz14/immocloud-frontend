// map the value of the status taken from backend to the enum of BewerberStatus
export function getEnumValueByKey(enumType: any, key: string) {
  return enumType[key as keyof typeof enumType]
}

// get the keys for enum to be sent for backend
export function getEnumKeyByValue(enumObj: any, enumValue: string): string | null {
  const keys = Object.keys(enumObj).find((key) => enumObj[key] === enumValue)
  return keys ?? null // Return null if the key was not found
}
